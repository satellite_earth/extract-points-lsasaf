
"""
This package allows you to extract a point from satellite hdf5 (e.g. LSASAF)
and save it to netCDF file.

:Structure HdfWorker: This class allows you to get the dataset/s from HDF5 file
                      and a value from point (lin, col). Also this class allows
                      you to get a dataframe:
                      e.g. 1var :: {"time": time_array, "var1": var_array}
                      e.g. 2var :: {"time": time_array, "var1": var_array,
                                    "var2: var_array}

           SavePoints: This class is used to save array to netCDF with values
                       obtained from a HDF5 point (lin, col). Also, it allows the
                       users append new variable to file out (netcdf).

Author: Nuno Simoes
"""

from pathlib import Path
import re
import logging
from datetime import datetime as dt
from datetime import timedelta as td
import json
import h5py
import numpy as np
import pandas as pd
import xarray as xr

# logger; formatter log; set configurations and get logger
logger = logging.getLogger()
formatter = '[%(asctime)s - %(filename)s:%(lineno)s - '\
            '%(funcName)s() - %(levelname)s ] %(message)s'
logging.basicConfig(format=formatter, level='INFO')


class SavePoints:
    """SavePoints object is used to save array to netCDF with values obtained from
    a HDF5 point (lin, col).
    Example:
        from extract_point import SavePoints
        s_points = SavePoints() # Please check the conf-file argument
                                if you want other file.
        s_points.save_points_to_netcdf(user_var, lon, lat, date_pd, file_out)

        # Append new variables
        s_points.append_to_netcdf({"em": em_array, "dslf": dslf_array})

    :param conf_file: filename with attributes for netCDF, by default is
                      "conf-nc-attrs.json".
    """
    def __init__(self, conf_name="conf-nc-attrs.json", file_out=None):
        self.nc_conf_name = conf_name
        self.conf_nc_dict = self.load_nc_conf_attr()
        self.file_out = file_out

    def append_to_netcdf(self, user_var):
        """Append new variable array to file out (netcdf).

        :param user_var: {} with user variables (can one or multiple)
            e.g. 1 variable: {"var1": np.array([10., 20., 100.])}
                 2 variable: {"var1": np.array([10., 20., 100.]),
                              "var2": np.array([10., 20., 100.])}
        Note: the var_name (var1) has to be the same that the key in conf file
        :return: append new variables to file out netcdf
        """
        try:
            # Prepare user variables
            d_user_var = {
                k: (['time'], user_var[k], self.conf_nc_dict[k])
                for k in user_var.keys()
            }
            df = xr.Dataset(d_user_var)
            df.to_netcdf(self.file_out, mode="a")
        except Exception as e:
            logger.error(e)

    def save_points_to_netcdf(self, user_var, lon=None, lat=None,
                              date_pd=None):
        """Save the values (array) obtained from each point (lon, lat)
         to the netcdf file.


        :param user_var: {} with user variables (can one or multiple)
            e.g. 1 variable: {"var1": np.array([10., 20., 100.])}
                 2 variable: {"var1": np.array([10., 20., 100.]),
                              "var2": np.array([10., 20., 100.])}
        Note: the var_name (var1) has to be the same that the key in conf file
        :param lon: longitude tuple e.g. [value]
        :param lat: latitude tuple e.g. [value]
        :param date_pd: datetime pandas array
                        (e.g. pd.date_range('YYYY-MM-DD', periods=3))

        :return: save output file in netcdf
        """
        # Prepare user variables
        user_var = {
            k: (['time'], user_var[k], self.conf_nc_dict[k])
            for k in user_var.keys()
        }
        # dictionary with basic variables (lon,lat,time,crs)
        d_variables = {'lon': (['lon'], lon, self.conf_nc_dict["lon_attr"]),
                       'lat': (['lat'], lat, self.conf_nc_dict["lat_attr"]),
                       'time': (['time'], date_pd, self.conf_nc_dict["time_attr"]),
                       'crs': 'i4'}
        # append user variables
        d_variables.update(user_var)

        # Create a xarray dataset
        ds = xr.Dataset(d_variables, attrs=self.conf_nc_dict["general_attr"])
        # Append crs attributes
        ds["crs"].attrs.update(self.conf_nc_dict["crs_attr"])
        # Save netcdf file
        ds.to_netcdf(self.file_out, format="NETCDF4")

    def available_attr_variables(self):
        """Check the attributes variables available.

        :return: list with keys variables.
        """
        try:
            return list(self.conf_nc_dict.keys())
        except Exception as e:
            logger.error(e)

    def load_nc_conf_attr(self):
        """Load netCDF attributes from configuration file.
        The file (conf-nc-attrs.json) has to be in the same code directory.

        :return: {} with attributes
        """
        try:
            with open(Path(__file__).resolve().parent / self.nc_conf_name) as f_conf:
                return json.load(f_conf)
        except Exception as e:
            logger.error(e)


class HdfWorker:
    """HdfWorker allows you to get the dataset from HDF5 file and a value from
    point (lin, col).

    Example1:
        from extract_point import HdfWorker

        # obejct - by default originals=True: original data; if you to
        # want apply missing_value and scale_factor please set originals=False.
        # NOTE: one dset_names e.g ["LST"]; multiple dsets ["LST", "Q_FLAGS", "errorbar_LST"]
        # we recommend get main variable, q_flags and errorbar
        hdf = HdfWorker(files=files, dset_names=dataset_names,
                        lin=lin_value, col=col_value)

        # get values from one file [file1] or for a list of files in [file1, file2, ....]
        data_frame = hdf.get_points()

        # You can use
        # NOTE: you can get min_value from get_lin_actime(); please see the docs for + info
        hdf = HdfWorker(files=[file1], dset_names=dataset_names,
                        lin=lin_value, col=col_value, line_actime_min=min_value)
        dset = hdf.get_hdf_dataset(file1) # Please check input arguments if you want
                                            masked array with missing_value and scale_factor
        point_value = hdf.get_point_from_dset(dset)

     :param files: hdf files - [file1] or [file1, file2, ....]
     :param dset_names: Dataset names - list str [dset1, dset2, ....]
     :param lin: line value - int
     :param col: column value - int
     :param line_actime_min: - int - difference between (line acquisition time - file_datetime)
                             in minutes (This value is important to get the true datetime from
                             pixel acquisition); That value is practically constant all over
                             the year.
                             By default this param is 0 but you can get it from get_lin_actime().
    :param originals: True (default) you get the original data (more fast)
                      False the package applies the missing_value and the scale_factor.
                      NOTE: If you set True, don t worry you just need put the correct values
                      by dataset in conf-nc-attrs.json and when you will open the netCDF this
                      values (missing_value and scale_factor) are applied.
    """
    def __init__(self, files, dset_names, lin=None, col=None, line_actime_min=0, originals=True):
        self.files = files
        self.dset_names = dset_names
        self.lin = lin
        self.col = col
        self.line_actime_min = line_actime_min
        self.originals = originals

    def get_points(self):
        """This function returns a pandas DataFrame with a time/s and value/s
        from a point location.
        This call other two functions: get_hdf_dataset() and get_point_from_dset().

        :return: pandas DataFrame {"time": time_array, "var": var_array}
        """
        try:
            # create dictionaries with dataset names and time
            d = {i: [] for i in self.dset_names}
            t_var = {"time": []}
            # Loop over files and get pd.time and dataset/s values from col,lin
            for f in sorted(self.files):
                t_var["time"].append(pd.to_datetime(self.get_timeslot(f))+td(minutes=self.line_actime_min))
                [d[dname].append(
                    self.get_point_from_dset(self.get_hdf_dataset(f, dname, self.originals)))
                    for dname in d.keys()]
            # update dataset/s dictionary to time dictionary; return DataFrame
            t_var.update(d)
            return pd.DataFrame(data=t_var)
        except Exception as e:
            logger.error(e)

    @staticmethod
    def get_timeslot(file_hdf):
        """This is a static method and it is just used to return the timeslot from filename.

        :param file_hdf: hdf file
        :return: timeslot from sile - str YYYYMMDDHHSS
        """
        try:
            return re.search(r'\d{4}\d{2}\d{2}\d{2}\d{2}', file_hdf).group()
        except Exception as e:
            logger.error(e)

    def get_point_from_dset(self, dset=None):
        """Get point for a specific location (lin,col) from dataset

        :param dset: dataset numpy array (from get_hdf_dataset())
        :return: point value
        """
        try:
            return dset[self.lin, self.col]
        except Exception as e:
            logger.error(e)

    @staticmethod
    def get_hdf_dataset(file_hdf, dset_name, originals=True):
        """Get HDF5 dataset.

        :param file_hdf :: hdf5 file
        :param dset_name :: dataset name - str
        :param originals :: boolean - False (apply mask to missing value and apply
                                      scale factor)
                             - True (default: originals data without mask and
                                     scale factor)
        :return: dset_value: dataset numpy array
        """
        try:
            # Read filename
            f = h5py.File(file_hdf, "r")
            # Load dataset
            dset = f[dset_name]
            # if data without mval and sfactor originals is True
            if originals:
                dset_value = dset.value
                f.close()
                return dset_value
            else:
                # Get missing value and scaling factor
                mval = dset.attrs["MISSING_VALUE"]
                sfac = dset.attrs["SCALING_FACTOR"]
                dset_value = np.ma.masked_values(dset.value, mval)/sfac
                f.close()
                return dset_value
        except Exception as e:
            logger.error(e)


def get_lin_actime(filename=None, lin=None):
    """Get line of acquisition time of your point. You have to request this file
    because this information is obtained from internal products. However, this
    information is practically constant all over the year. Therefore, you just need
    to get this parameter one time.

    :param filename: file (HDF5) with Lines_Acquisition_Time dataset.
    :param lin: point line
    :return: minutes difference between file_datetime - line_datetime
    """
    try:
        # Open h5 file
        f = h5py.File(filename, "r")
        # get line datetime object (convert np.int64 to datetime)
        lin_date = dt(*f["Lines_Acquisition_Time"].value[lin])
        # close file
        f.close()

        # Get file datetime and compute the diff between f_date and line_date
        # in order to get minute difference between both dates, this difference
        # is practically constant all over the year.
        file_date = pd.to_datetime(HdfWorker.get_timeslot(filename))
        min_diff = int((lin_date - file_date).total_seconds() / 60.)
        logger.info("File time: %s; Line acq_time for lin %i is %s; Minutes difference: %i"
                    % (file_date, lin, lin_date, min_diff))
        return min_diff
    except Exception as e:
        logger.error(e)
