
"""Test extract point usb lib"""

import pytest
import numpy as np
import pandas as pd

from satpoint import extract_point as ep

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("file_hdf, tslot_expected", [
    ("HDF5_LSASAF_MSG_PROD_MSG-Disk_201809041000", "201809041000"),
    ("HDF5_120_MSG_TB_MSG-Disk_201810021000", "201810021000"),
    ("HDF5_LSASAF_MSG_PROD_Euro_201504041000", "201504041000"),
    ("HDF5_LSASAF_MSG_PROD_SAfr_201504041000", "201504041000"),
    ("HDF5_LSASAF_MSG_PROD_SAme_201504041000", "201504041000"),
    ("HDF5_LSASAF_MSG_PROD_NAfr_201504041000", "201504041000")
])
def test_get_timeslot(file_hdf, tslot_expected):
    """Test timeslot from get_timeslot().

    :param file_hdf: HDF5 LSASAF format
    :param tslot_expected: timeslot YYYYMMDDHHMM str
    :return: assert result
    """
    tslot = ep.HdfWorker.get_timeslot(file_hdf)
    assert tslot == tslot_expected


@pytest.mark.parametrize("file_hdf, dset_name, kind_data, shape_expected, type_expected", [
    ("/DATA/HDF5_LSASAF_MSG_LST_MSG-Disk_201510251200", "LST", False, (3712, 3712), np.ma.core.MaskedArray),
    ("/DATA/HDF5_LSASAF_MSG_LST_MSG-Disk_201510251200", "LST", True, (3712, 3712), np.ndarray)
])
def test_get_hdf_dataset(file_hdf, dset_name, kind_data, shape_expected, type_expected):
    """Test shape and type from get_hdf_dataset()

    :param file_hdf: HDF5 LSASAF filename (used file in DATA)
    :param dset_name: dataset name
    :param kind_data: False=with mask and scale_factor True: originals
    :param shape_expected: dataset shape(3712, 3712)
    :param type_expected: np.ma.core.MaskedArray or np.ndarray
    :return: assert result
    """
    dset = ep.HdfWorker.get_hdf_dataset(file_hdf, dset_name, originals=kind_data)
    assert dset.shape == shape_expected
    assert type(dset) == type_expected


@pytest.mark.parametrize("file_hdf, dsets, lin, col, lin_min, kind_data, lst_out, ebar_out, qf_out, t_out", [
    ("/DATA/HDF5_LSASAF_MSG_LST_MSG-Disk_201510251200", ["LST", "errorbar_LST", "Q_FLAGS"],
        630, 1606, 10, False, np.ma.masked_values(np.array([-8000.]), -8000.),
        np.ma.masked_values(np.array([-1]), -1), 60.0, pd.Timestamp('2015-10-25 12:10:00')),
    ("/DATA/HDF5_LSASAF_MSG_LST_MSG-Disk_201510251200", ["LST", "errorbar_LST", "Q_FLAGS"], 630, 1606, 10, True,
        -8000, -1, 60, pd.Timestamp('2015-10-25 12:10:00')),
])
def test_get_hdf_dataset(file_hdf, dsets, lin, col, lin_min, kind_data, lst_out, ebar_out, qf_out, t_out):
    """Test get_hdf_dataset().

    :param file_hdf: file HDF5 LSASAF (used file in DATA)
    :param dsets: dsets ["LST", "errorbar_LST", "Q_FLAGS"]
    :param lin: pixel line e.g. 630 just to test
    :param col: pixel column e.g. 1606 just to test
    :param lin_min: pixel line acquisition time (dif minutes f_date-pixel_ac_time) e.g. 10
    :param kind_data: False=with mask and scale_factor True: originals
    :param lst_out: kind_data_False=masked; kind_data_True=-8000
    :param ebar_out: kind_data_False=masked; kind_data_True=-1
    :param qf_out: kind_data_False=60.0; kind_data_True=60
    :param t_out: file test - Timestamp('2015-10-25 12:10:00')
    :return: assert result
    """
    d = ep.HdfWorker(files=[file_hdf], dset_names=dsets, lin=lin, col=col,
                     line_actime_min=lin_min, originals=kind_data)
    v = d.get_points()
    assert v["LST"].values[0] == lst_out
    assert v["errorbar_LST"].values[0] == ebar_out
    assert v["Q_FLAGS"].values[0] == qf_out
    assert v["time"][0] == t_out
