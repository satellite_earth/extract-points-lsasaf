
"""Test for coords_converter"""

import pytest

from coords_converter import get_col_lin_from_sat as get_coords

pytestmark = pytest.mark.unit


@pytest.mark.parametrize("region_str, lon_f, lat_f, col_lin_expected", [
    ("MSG-Disk", -8.804837, 37.142934, (1606, 630)),
])
def test_col_lin(region_str, lon_f, lat_f, col_lin_expected):
    p = get_coords.CoordsConverter(region=region_str, lon=lon_f, lat=lat_f)
    col_lin = p.get_collin_from_latlon()
    assert col_lin == col_lin_expected