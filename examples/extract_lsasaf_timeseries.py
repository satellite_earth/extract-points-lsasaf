
"""Get TimeSeries from LSASAF HDF5 product and save it to netCDF.

   How to use: - python extract_lsasaf_timeseries.py -coords lat lon
        -r region -f folder_with_hdf -d dataset names -f_out file_out
        -ac_f file with ac time -lmin min_dif_f_date_pixel_date
        NOTE: -c_f conf_name not required if you want add new conf file
                    has to be in the configuration folder and you just
                    give the filename.
              -ac_f file with acquisition time (request lsasaf) and optional
                    argument;
              -lmin dif between f_date and pixel_date in minutes and it is
                    a optional argument. if you provide this, the
                    ac_f arg is not needed.

        ipython: - from extract_lsasaf_timeseries import point_hdf2nc
                 - point_hdf2nc(region, lon, lat, path_data, dset_names,
                                file_out, lin_min, lin_ac_file)

   Author: Nuno Simoes
"""

import argparse
from pathlib import Path
import logging
import glob
from math import sqrt, pow

from satpoint import extract_point as ep
from coords_converter import get_col_lin_from_sat as get_coords


_LOG_LEVEL_STRINGS = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
logger = logging.getLogger()


def point_hdf2nc(region, lon, lat, path_data, dset_names, file_out, lin_min, lin_ac_file=None):
    """This function convert the lon,lat to col,lin; get the correct pixel
    acquisition time; get the point values for all variables and for all files;
    and finally save it into netCDF TimeSeries.

    :param region: Region names for get col,lin from lon,lat: MSG-Disk,Euro,NAfr,SAfr,SAme
    :param lon: point longitude - epsg:4326
    :param lat: point latitude - epsg:4326
    :param path_data: path with your HDF5 files
    :param dset_names: list with your dataset names e.g ["LST","errorbar_LST","Q_FLAGS"]
    :param file_out: output file in netCDF
    :param lin_min: difference between f_date and pixel_acquisition time in minutes,
                    if you do not provide this, by default is 0 in command line and in
                    ipython please set this variable to 0. The point acquisition
                    time is practically constant all over the year.
    :param lin_ac_file: file with Lines_Acquisition_Time dataset (please request this file).
                        If you already know the minutes please set this variable as None
                        and set lin_min with correct minutes.
    :return: This script will save the netCDF TimeSeries.
    """
    # Get col,lin from lon,lat
    p = get_coords.CoordsConverter(region=region, lon=lon, lat=lat)
    col, lin = p.get_collin_from_latlon()

    # if acquisition file get minute difference (file_date-pixel_date)
    if lin_ac_file:
        lin_min = ep.get_lin_actime(lin_ac_file, lin)

    # Get files
    files = glob.glob(path_data + "/HDF5_*")

    # call HdfWorker to get point in dataframe from your file/s
    hdf = ep.HdfWorker(files=files, dset_names=dset_names, lin=lin, col=col,
                       line_actime_min=lin_min, originals=True)
    df = hdf.get_points()

    # Save point (DataFrame) to netCDF
    s_point = ep.SavePoints(file_out=file_out)

    # create dictionary with user var/s and save it to netCDF
    user_vars = {i: df[i].values for i in dset_names}
    s_point.save_points_to_netcdf(user_var=user_vars,
                                  lon=[lon], lat=[lat],
                                  date_pd=df["time"])


def _log_level_string_to_int(log_level_string):
    """Test log leve string given in argparse.

    :param log_level_string: ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
    :return: log level int
    """
    if not (log_level_string in _LOG_LEVEL_STRINGS):
        message = 'invalid choice: {0} (choose from'\
                      ' {1})'.format(log_level_string, _LOG_LEVEL_STRINGS)
        raise argparse.ArgumentTypeError(message)
    log_level_int = getattr(logging, log_level_string, logging.INFO)
    # check the logging log_level_choices have not changed
    # from our expected values
    assert isinstance(log_level_int, int)
    return log_level_int


def _test_coords(coords):
    """Test Coordinates.

    :param coords: [lat, lon]
    :return: lat, lon
    """
    if sqrt(pow(coords[0], 2) + pow(coords[1], 2)) > 80:
        logger.error("Latitude: %f and Longitude: %f are None!"
                     % (coords[0], coords[1]))
        return None, None
    else:
        logger.info("Latitude: %f and Longitude: %f are Ok!"
                    % (coords[0], coords[1]))
        return coords[0], coords[1]


def _is_valid_dir(parser, arg):
    """Test if input dir or output dir is valid.
    This function is used with argparser.

    :param parser: parser from argparse
    :param arg: directory
    :return: dir or error message
    """
    return arg if Path(arg).is_dir() else\
        parser.error("The directory %s does not exist! " % arg)


def _prepare_args():
    # get conf filename from argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-conf", "--conf_name", help="Configuration name"
    )
    parser.add_argument(
        "-coords", "--coords", required=True, nargs=2,
        help="Coordinates: lat lon in WGS84 epsg:4326", type=float
    )
    parser.add_argument(
        "-r", "--region", required=True,
        help="Region names: MSG-Disk, Euro, NAfr, SAfr, SAme"
    )
    parser.add_argument(
        "-f", "--input_folder", help="input folder",
        type=lambda x: _is_valid_dir(parser, x)
    )
    parser.add_argument(
        "-d", "--dsets", required=True, nargs='*',
        help="Dataset names e.g. LST, errorbar_LST, Q_FLAGS", type=str
    )
    parser.add_argument(
        "-f_out", "--output_file", required=True,
        help="output filename", metavar="FILE"
    )
    parser.add_argument(
        "-ac_f", "--ac_file", metavar="FILE",
        help="file with Lines_Acquisition_Time"
    )
    parser.add_argument(
        "-lmin", "--lin_min", type=int, default=0,
        help="difference pixel_date - file_date in minutes,"
             " you can obtain this from extract_point.get_lin_actime()"
    )
    parser.add_argument('-v', '--log_level', default='INFO', dest='log_level',
                        type=_log_level_string_to_int, nargs='?',
                        help='Set the logging output'
                        ' level. {0}'.format(_LOG_LEVEL_STRINGS))
    args = parser.parse_args()

    # formatter log; set configurations and get logger
    formatter = '[%(asctime)s - %(filename)s:%(lineno)s - ' \
                '%(funcName)s() - %(levelname)s ] %(message)s'
    logging.basicConfig(format=formatter, level=args.log_level)

    # Test coords and get lat,lon
    lat, lon = _test_coords(args.coords)

    # Call main script
    point_hdf2nc(args.region, lon, lat, args.input_folder, args.dsets,
                 args.output_file, args.lin_min, args.ac_file)


if __name__ == "__main__":
    _prepare_args()
