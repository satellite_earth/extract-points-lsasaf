
"""Get TimeSeries from ECMWF files and save it to netCDF.

   How to use: - Get daily timeseries from ecmwf files
                 python extract_ecmwf_timeseries.py -coords 37.142934 -8.804837
                 -m "nearest" -f input_folder -d "STR" -f_out out_file
                 -si "0000" -sf "2353" -hs 24

                ipython: - from extract_ecmwf_timeseries import point_ecmwf2nc
                 - point_ecmwf2nc(path_data, lon, lat, method, dset_names,
                                file_out, stepi_h, stepf_h, hours)

   Author: Nuno Simoes
"""

import argparse
from pathlib import Path
import logging
import glob
from math import sqrt, pow

from p_ecmwf import point_ecmwf as pe
from satpoint import extract_point as ep


_LOG_LEVEL_STRINGS = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
logger = logging.getLogger()


def point_ecmwf2nc(path_data, lon, lat, method, dset_names, file_out):
    """Get point value/s from file/s and save it to NetCDF.
    e.g. from extract_ecmwf_timeseries import point_ecmwf2nc
         point_ecmwf2nc(path_data, lon, lat, method, dset_names,
                      file_out, stepi_h, stepf_h, hours)

    :param path_data: folder with data (folder/YYYYMM/DATA/) you just give folder name
    :param lon: longitude value epsg: 4326
    :param lat: latitude value epsg: 4326
    :param method: interpolation method to get point location e.g. nearest
    :param dset_names: variable/s name ["STR"] or ["STR", "SVR"]
    :param file_out: output filename
    :return: save netcdf file with point value/s
    """
    # Get files
    files = glob.glob(path_data + "/**/*")

    # call Pecmwf to get point in dataframe from your file/s
    ds = pe.Pecmwf(files, dset_names, lon, lat, method)
    df = ds.get_d_timeseries()

    # Save point (DataFrame) to netCDF
    s_point = ep.SavePoints(file_out=file_out)

    # create dictionary with user var/s and save it to netCDF
    user_vars = {i: df[i].values for i in dset_names}
    s_point.save_points_to_netcdf(user_var=user_vars,
                                  lon=[lon], lat=[lat],
                                  date_pd=df["time"])


def _log_level_string_to_int(log_level_string):
    """Test log leve string given in argparse.

    :param log_level_string: ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
    :return: log level int
    """
    if not (log_level_string in _LOG_LEVEL_STRINGS):
        message = 'invalid choice: {0} (choose from'\
                      ' {1})'.format(log_level_string, _LOG_LEVEL_STRINGS)
        raise argparse.ArgumentTypeError(message)
    log_level_int = getattr(logging, log_level_string, logging.INFO)
    # check the logging log_level_choices have not changed
    # from our expected values
    assert isinstance(log_level_int, int)
    return log_level_int


def _test_coords(coords):
    """Test Coordinates.

    :param coords: [lat, lon]
    :return: lat, lon
    """
    if sqrt(pow(coords[0], 2) + pow(coords[1], 2)) > 80:
        logger.error("Latitude: %f and Longitude: %f are None!"
                     % (coords[0], coords[1]))
        return None, None
    else:
        logger.info("Latitude: %f and Longitude: %f are Ok!"
                    % (coords[0], coords[1]))
        return coords[0], coords[1]


def _is_valid_dir(parser, arg):
    """Test if input dir or output dir is valid.
    This function is used with argparser.

    :param parser: parser from argparse
    :param arg: directory
    :return: dir or error message
    """
    return arg if Path(arg).is_dir() else\
        parser.error("The directory %s does not exist! " % arg)


def _prepare_args():
    # get conf filename from argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-coords", "--coords", required=True, nargs=2,
        help="Coordinates: lat lon in WGS84 epsg:4326", type=float
    )
    parser.add_argument(
        "-m", "--method", required=True,
        help="interpolation method to get point location e.g. nearest"
    )
    parser.add_argument(
        "-f", "--input_folder", help="input folder",
        type=lambda x: _is_valid_dir(parser, x)
    )
    parser.add_argument(
        "-d", "--dsets", required=True, nargs='*',
        help="Dataset names e.g. LST, errorbar_LST, Q_FLAGS", type=str
    )
    parser.add_argument(
        "-f_out", "--output_file", required=True,
        help="output filename", metavar="FILE"
    )
    parser.add_argument(
        "-hs", "--hours", type=int,
        help="the interval between date_i and date_f this is used to convert "
             "accumulated values to daily mean by seconds"
    )
    parser.add_argument('-v', '--log_level', default='INFO', dest='log_level',
                        type=_log_level_string_to_int, nargs='?',
                        help='Set the logging output'
                        ' level. {0}'.format(_LOG_LEVEL_STRINGS))
    args = parser.parse_args()

    # formatter log; set configurations and get logger
    formatter = '[%(asctime)s - %(filename)s:%(lineno)s - ' \
                '%(funcName)s() - %(levelname)s ] %(message)s'
    logging.basicConfig(format=formatter, level=args.log_level)

    # Test coords and get lat,lon
    lat, lon = _test_coords(args.coords)

    # Call main script
    point_ecmwf2nc(args.input_folder, lon, lat, args.method, args.dsets, args.output_file)


if __name__ == "__main__":
    _prepare_args()
