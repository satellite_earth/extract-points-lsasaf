
"""Description: Get Col,Lin from Lat,Lon for specific Area and Satellite.

   How to use: - python get_col_lin_from_sat.py -c_f conf_file -coods lat lon
                                                -r region_name

               - from get_col_lin_from_sat import CoordsConverter
                 coords_conv = CoordsConverter(conf_name, region, lon, lat)
                 col,lin = coords_conv.get_collin_from_latlon()

   Author: Nuno Simoes
"""

from math import pow, asin, sin, cos, atan, tan, sqrt, degrees, radians
import argparse
import logging
from pathlib import Path
import json


_LOG_LEVEL_STRINGS = ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
logger = logging.getLogger()


class CoordsConverter:
    """Description: get col,lin from lat,lon coordinates - Geostationary Sat.
                    This is usefull when you can obtain the value from a
                    specific point.

    :param conf_name: filename with configuration (json) (not required)
                    when you give this argument it has to be in configuration folder.
    :param region: region name (MSG-Disk, Euro, NAfr, SAfr, SAme)
    :param lon: longitude - epsg:4326 - int
    :param lat: latitude - epsg:4326 - int

    :return col, lin: column and line - int
    """
    def __init__(self, conf_name="config-col-lin.json", region=None, lon=None, lat=None):
        self.conf_attrs_name = conf_name
        self.region = region
        self.lon = lon
        self.lat = lat
        self.cd = self.load_json()
        # Load AUX_PARAMETERS
        self.aux16 = pow(float(self.cd["AUX_PARAMETERS"]["aux16"][0]), float(self.cd["AUX_PARAMETERS"]["aux16"][1]))
        self.p1 = self.cd["AUX_PARAMETERS"]["p1"]
        self.p2 = self.cd["AUX_PARAMETERS"]["p2"]
        self.p3 = self.cd["AUX_PARAMETERS"]["p3"]
        self.rp = self.cd["AUX_PARAMETERS"]["rp"]
        # Load Area Parameters
        self.CFAC = self.cd["AREA"][self.region]["CFAC"]
        self.LFAC = self.cd["AREA"][self.region]["LFAC"]
        self.sub_lon = self.cd["AREA"][self.region]["sub_lon"]
        self.COFF = self.cd["AREA"][self.region]["COFF"]
        self.LOFF = self.cd["AREA"][self.region]["LOFF"]
        self.col_min = self.cd["AREA"][self.region]["col_min"]
        self.col_max = self.cd["AREA"][self.region]["col_max"]
        self.lin_min = self.cd["AREA"][self.region]["lin_min"]
        self.lin_max = self.cd["AREA"][self.region]["lin_max"]

    def available_areas(self):
        """Get available areas.
        """
        try:
            return list(self.cd["AREA"].keys())
        except Exception as e:
            logger.error(e)

    def load_json(self):
        """Load json with configurations.

        :return:
        """
        try:
            with open(Path(__file__).resolve().parent / self.conf_attrs_name) as f:
                return json.load(f)
        except Exception as e:
            logger.error(e)

    def get_collin_from_latlon(self):
        """Get col,lin from lat,lon for specific area and satellite.
           These equations were obtained from LSASAF website,
           for more information please see the documentation
           or contact the LSASAF helpdesk.

        :return: col,lin
        """
        # convert degrees to radians
        lat_rad = radians(self.lat)
        lon_rad = radians(self.lon)
        # compute auxiliary variables
        c_lat = atan(self.p3 * tan(lat_rad))
        rl = self.rp / sqrt(1. - self.p2 * pow(cos(c_lat), 2))
        r1 = self.p1 - rl * cos(c_lat) * cos(lon_rad - self.sub_lon)
        r2 = -rl * cos(c_lat) * sin(lon_rad - self.sub_lon)
        r3 = rl * sin(c_lat)
        rn = sqrt(r1**2 + r2**2 + r3**2)
        # get x and y
        x = atan(-r2/r1)
        y = asin(-r3/rn)
        # convert again to degrees
        x = degrees(x)
        y = degrees(y)
        # get col,lin
        col = int(self.COFF + round(x * self.aux16 * self.CFAC))
        lin = int(self.LOFF + round(y * self.aux16 * self.LFAC))

        # Check if col,lin are inside the Areas limiters
        if (self.col_min-5 <= col <= self.col_max+5) and (self.lin_min-5 <= lin <= self.lin_max+5):
            return col-1, lin-1
        else:
            logger.error("The col %i and lin %i do not belong to this"
                         " area %s" % (col, lin, self.region))
            return None, None


def _is_valid_file(parser, arg):
    """Test if input conf file is valid.
       This function is used with argparser.

    :param parser: parser from argparse
    :param arg: file
    :return: file if is exists
    """
    return arg if Path(arg).is_file() else\
        parser.error("The file %s does not exist! " % arg)


def _log_level_string_to_int(log_level_string):
    """Test log leve string given in argparse.

    :param log_level_string: ['CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']
    :return: log level int
    """
    if not (log_level_string in _LOG_LEVEL_STRINGS):
        message = 'invalid choice: {0} (choose from'\
                      ' {1})'.format(log_level_string, _LOG_LEVEL_STRINGS)
        raise argparse.ArgumentTypeError(message)
    log_level_int = getattr(logging, log_level_string, logging.INFO)
    # check the logging log_level_choices have not changed
    # from our expected values
    assert isinstance(log_level_int, int)
    return log_level_int


def _test_coords(coords):
    """Test Coordinates.

    :param coords: [lat, lon]
    :return: lat, lon
    """
    if sqrt(pow(coords[0], 2) + pow(coords[1], 2)) > 80:
        logger.error("Latitude: %f and Longitude: %f are None!"
                     % (coords[0], coords[1]))
        return None, None
    else:
        logger.info("Latitude: %f and Longitude: %f are Ok!"
                    % (coords[0], coords[1]))
        return coords[0], coords[1]


def _prepare_args():
    """Description: prepare_args.
    """
    # get conf filename from argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-c_f", "--conf_file", help="Configuration name"
    )
    parser.add_argument(
        "-coords", "--coords", required=True, nargs=2,
        help="Coordinates: lat lon in WGS84 epsg:4326",
        type=float
    )
    parser.add_argument(
        "-r", "--region", required=True,
        help="Region names: MSG-Disk, Euro, NAfr, SAfr, SAme"
    )
    parser.add_argument('-v', '--log_level', default='INFO', dest='log_level',
                        type=_log_level_string_to_int, nargs='?',
                        help='Set the logging output'
                             ' level. {0}'.format(_LOG_LEVEL_STRINGS))
    args = parser.parse_args()

    # formatter log; set configurations and get logger
    formatter = '[%(asctime)s - %(filename)s:%(lineno)s - '\
                '%(funcName)s() - %(levelname)s ] %(message)s'
    logging.basicConfig(format=formatter, level=args.log_level)

    # Test coords and get lat,lon
    lat, lon = _test_coords(args.coords)
    logger.info("Conf_file: %s; Lat: %f; Lon: %f; Region: %s!"
                % (args.conf_file, lat, lon, args.region))

    # call get_col_lin calss and get col,lin from lat,lon
    collin_conv = CoordsConverter(args.conf_name, args.region, lon, lat)
    col, lin = collin_conv.get_collin_from_latlon()
    logger.info("Col: %i and Lin: %i " % (col, lin)) if (col and lin) else\
        logger.error("Col and Lin are None! Please give coordinates inside "
                     "the area: %s" % args.region)


if __name__ == "__main__":
    _prepare_args()
