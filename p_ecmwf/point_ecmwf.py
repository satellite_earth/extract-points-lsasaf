
"""
This package allows you get a point value/s (timeseries) from ECMWF NetCDF file/s.

:Structure Pecmwf: obtain point value/s (timeseries) from file/s
                   e.g. get daily means from point location
                   p = Pecmwf([file/s], ["STR"], lon, lat, "nearest")
                   ts = p.get_d_timeseries()

Author: Nuno Simoes
"""

import re
import logging
import pandas as pd
import xarray as xr

# logger; formatter log; set configurations and get logger
logger = logging.getLogger()
formatter = '[%(asctime)s - %(filename)s:%(lineno)s - '\
            '%(funcName)s() - %(levelname)s ] %(message)s'
logging.basicConfig(format=formatter, level='INFO')


class Pecmwf:
    """The main function of this class is to obtain a point value/s (timeseries) from file/s.

    e.g. Get daily mean from point location.
    p = Pecmwf([file/s], ["STR"], lon, lat, "nearest")
    ts = p.get_d_timeseries()

    :param files: file or files e.g. [file] or [files]
    :param var_names: variables name e.g. ["STR"] or ["STR", "SVR"]
    :param lon: longitude float - epsg: 4326
    :param lat: latitude float - epsg: 4326
    :param method: method used to get point location e.g. nearest
    :param hours: int - hours - default(24H) is the interval between date_i and date_f this is
    used to convert accumulated values to daily by seconds
    """
    def __init__(self, files, var_names, lon, lat, method, hours=24):
        self.files = files
        self.var_names = var_names
        self.lon = lon
        self.lat = lat
        self.method = method
        self.hours = hours
        self.step_tsloti = "0000"
        self.step_tslotf = 1
        self.seconds = 3600.

    def get_d_timeseries(self):
        """Get a daily point (lon,lat) timeseries from a file or multiple files.

        :return: pandas DataFrame {"time": time_array, "var": var_array}
        """
        try:
            # create dictionaries with dataset names and time
            d = {i: [] for i in self.var_names}
            t_var = {"time": []}
            # Loop over files and get pd.time and dataset/s values from lon,lat
            for f in sorted(self.files):
                # tslot_i is YYYYMMDD file slot for HHMM:0000
                # tlost_f is tslot_i more onde day in order to get the variable to full day
                tslot_i = pd.to_datetime(get_timeslot(f)+self.step_tsloti)
                tslot_f = tslot_i + pd.DateOffset(days=self.step_tslotf)
                t_var["time"].append(pd.to_datetime(get_timeslot(f)))
                [d[dname].append(
                    self.get_point_from_ds(self.get_var_from_ecmwf(f, dname), dname,
                                           tslot_i, tslot_f))
                    for dname in d.keys()]
            # update dataset/s dictionary to time dictionary; return DataFrame
            t_var.update(d)
            return pd.DataFrame(data=t_var)
        except Exception as e:
            logger.error(e)

    def get_point_from_ds(self, var, dname, date_i, date_f):
        """Get point (lon,lat) by method given as argument. After this function
        apply the (date_f-date_i)/(24*3600.) because we are using the accumulated values.
        e.g. file_201810020800 if you want a daily value we give date_i:201810020000
        and date_f: 201810030000.

        :param var: DataArray from get_var_from_ecwmf()
        :param dname: dataset/variable name
        :param date_i: initial date
        :param date_f: final date
        :return: point mean for interval between date_i and date_f
        """
        try:
            # get point (lon,lat) dataframe from matrix using method given as argument
            p_df = var.sel(lon=self.lon, lat=self.lat, method=self.method).to_dataframe()
            # In order to get the daily value we apply (date_f - date_i)/(24*3600.)
            # because the file has accumulated values and we convert specific hours in seconds
            p_var = (p_df[p_df.index.to_pydatetime() == date_f][dname].values -
                     p_df[p_df.index.to_pydatetime() == date_i][dname].values) / (self.hours*self.seconds)
            return float(p_var)
        except Exception as e:
            logger.error(e)

    @staticmethod
    def get_var_from_ecmwf(filename, var_name):
        """Get DataArray from ECMWF NetCDF file for a specific Dataset.

        :param filename: ECMWF NetCDF file.
        :param var_name: Variable name
        :return: DataArray with variable given
        """
        try:
            # Open file and get datarray
            ds = xr.open_dataset(filename)
            var = ds[var_name]
            ds.close()
            return var
        except Exception as e:
            logger.error(e)


def get_timeslot(filename):
    """This is a static method and it is just used to return the timeslot from filename.

    :param filename: ecmwf nc file
    :return: timeslot from file - str YYYYMMDD
    """
    try:
        return re.search(r'\d{4}\d{2}\d{2}', filename).group()
    except Exception as e:
        logger.error(e)
