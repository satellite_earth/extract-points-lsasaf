extract-points-lsasaf
---------------------

This package allows you to get a value/s (from one or multiple files) from a
specific point from LSASAF HDF5 products and save it to a netCDF (TimeSeries).
As the HDF5 files are not projected, then with this package you can convert
(lon, lat) to (col, lin) for geostationary satellites.
Additionally, this package allows you to get a value/s (from file/s one or
multiple file/s) from a specific point - ECMWF NetCDF.


Installation:
-------------

### Requierements

    - Linux
    - Python >= 3.

### Basic Installation

.. code-block:: c

    pip install .

..


Usage:
------

**As a script:**


    python get_col_lin_from_sat.py -c_f conf_file -coords lat lon -r region


**As a module**

    *- Get satellite col,lin from lat,lon (HDF5 - LSASAF):*

.. code-block:: c

     from coords_converter import get_col_lin_from_sat

     # NOTE: You can provide other conf file (same folder configuration file) and for that
     # you just give as argument conf_name=filename (not required beacause by default is
     # conf-col-lin.json) e.g. CoordsConverter(conf_name="meu_conf_name.json", region, lon, lat)
     coords_conv = get_col_lin_from_sat.CoordsConverter(region, lon, lat)

     col,lin = coords_conv.get_collin_from_latlon()
..

    *- Get Value from point (lin, col) in HDF5 and save or append value/multiple_values to netCDF*

.. code-block:: c

    from satpoints import extract_point

    # get difference between file_date and pixel_ac_time in minutes in order to get the correct
    # pixel acquisition time
    lin_min = ep.get_lin_actime(file_with_ac_time, lin)

    # NOTE1: dset_names can be one ["LST"] or multiple datasets ["LST", "Q_FLAGS", "errorbar_LST"]
    # NOTE2: originals=True more fast than False (False apply missing_value and scale_factor)
    # NOTE3: line_actime_min: by default is 0; but you can obtain this values using get_lin_actime()
    # and please keep in mind this value is practically constant all over the year.
    hdf = extract_point.HdfWorker(filename=filename, dset_names=dataset_name,
                                  lin=lin_value, col=col_value,
                                  line_actime_min=lin_min, originals=True)

    # DataFrame: 1var: {"time": pd_time, "var": var_array}
    #            2var: {"time": pd_time, "var1": var_array, "var2": var_array}
    df = hdf.get_points()

    save_points = extract_point.SavePoints(file_out=file_out)

    # save dataframe (df) obtained in hdf.get_points() to netcdf
    save_points.save_points_to_netcdf(user_var={"var1": df["var1"],
                                                "var2": df["var2"]},
                                      lon=[lon_value], lat=[lat_value],
                                      date_pd=df["time"])

    # the arrays have to be the same dimensions than originals
    save_points.append_to_netcdf({"em": [100,200,300], "dslf": [200,300,400]})
..

    *- Get lon,lat value/s from ECMWF file/s (NetCDF):*

.. code-block:: c

     from p_ecmwf import point_ecmwf

     # lon,lat are epsg:4326 float coordinates; method: e.g. nearest - spatial interpolation
     # dset_names: e.g. ["STR"] or ["STR", "SVR"];
     # hours: int - hours - is the interval between date_i and date_f this is used to convert
     # accumulated values to daily mean by seconds
     ds = point_ecmwf.Pecmwf(path_data, lon, lat, method, dset_names, file_out)

     # get daily timeseries
     df = ds.get_d_timeseries()
..




