argparse==1.1
logging==0.5.1.2
numpy==1.15.2
h5py==2.8.0
json==2.0.9
pandas==0.23.4
xarray==0.10.9
re==2.2.1e